import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NewUserFormComponent } from './new-user-form/new-user-form.component';
import { UsersComponent } from './users/users.component';

const routes: Routes = [
  {
    path:'users',
    loadChildren:() => import('./users/users.module').then(m=>m.UsersModule),
    component:UsersComponent
  },
  {
    path:'new-user',
    component:NewUserFormComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
