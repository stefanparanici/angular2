import { Injectable } from '@angular/core';
import { User } from './users/users.component';


@Injectable({
  providedIn: 'root'
})
export class DataService {
  users:User[] = [];
  addUser(_name:string,_age:string,_email:string){
    this.users[this.users.length] = {name:_name,age:_age,Email:_email}
  }
}