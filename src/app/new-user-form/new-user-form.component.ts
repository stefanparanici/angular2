import { Component} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import {Validators} from '@angular/forms';
import { DataService } from '../data.service';


@Component({
  selector: 'app-new-user-form',
  templateUrl: './new-user-form.component.html',
  styleUrls: ['./new-user-form.component.scss']
})
export class NewUserFormComponent{
  myForm:FormGroup;
  constructor(private fb:FormBuilder,public usersDB:DataService)
  {
    this.myForm = this.fb.group({
      name:["",[Validators.required,
      Validators.minLength(3),
      Validators.maxLength(50)]],
      age:["",Validators.required],
      Email:["",Validators.email]
    });
  }
  
}