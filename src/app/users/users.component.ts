import { Component} from '@angular/core';
import { DataService } from '../data.service';

export interface User{
  name:string;
  age:string;
  Email:string;
}
@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent{
  users:User[];
  constructor(private usersDB:DataService){
    this.users = this.usersDB.users;
  }
  
}
