import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersComponent } from './users.component';
import { RouterModule } from '@angular/router';
import {MatCardModule} from '@angular/material/card';
import { NewUserFormModule } from '../new-user-form/new-user-form.module';


@NgModule({
  declarations: [
    UsersComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    MatCardModule,
    NewUserFormModule
  ]
})
export class UsersModule { }
